# RECAST

1. Copy the signal DOAD file into the `inputdata` directory, and rename it `recast_daod.root`.

2. To run the workflow:

```
yadage-run workdir workflow.yml inputs.yml -d initdir=$PWD/inputdata
cat workdir/fitting/result.json
```
